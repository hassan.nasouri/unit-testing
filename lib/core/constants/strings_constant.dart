class ConstantStrings {
  ConstantStrings._();

  static String appTitle = 'Unit Testing';
  static String carViewTitle = 'Car View';
  static String carListViewTitle = 'Car List View';
}
