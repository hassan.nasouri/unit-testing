import 'package:get_it/get_it.dart';
import 'package:unit_testing/core/services/api.dart';
import 'package:unit_testing/core/viewmodels/cart_model.dart';
import 'package:unit_testing/core/viewmodels/product_list_model.dart';

GetIt dependencyAssembler = GetIt.instance;

void setupDependencyAssembler() {
  dependencyAssembler.registerLazySingleton(() => API());
  dependencyAssembler.registerFactory(() => ProductListModel());
  dependencyAssembler.registerFactory(() => CartModel());
}
