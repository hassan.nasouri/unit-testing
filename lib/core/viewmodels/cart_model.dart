import 'package:unit_testing/core/models/product.dart';

import 'base_model.dart';

class CartModel extends BaseModel {
  List<Product> cart = [];
  Map<String, List<Product>> cartSummary = {};

  int get cartSize {
    return cart.length;
  }

  void addToCart(Product product) {
    cart.add(product);
    notifyListeners();
  }

  List<Product> getCart() {
    return cart;
  }

  Map<String, List<Product>> getCartSummary() {
    cartSummary = {};
    cart.forEach((product) {
      if (!cartSummary.keys.contains(product.name)) {
        if (product.name != null) cartSummary[product.name!] = [product];
      } else {
        var currentProducts = cartSummary[product.name];

        currentProducts?.add(product);

        if (product.name != null) cartSummary[product.name!] = currentProducts!;
      }
    });
    return cartSummary;
  }

  Product getProduct(int index) {
    var name = cartSummary.keys.elementAt(index);
    return cartSummary[name]!.first;
  }

  int? getProductQuantity(int index) {
    var name = cartSummary.keys.elementAt(index);
    return cartSummary[name]?.length;
  }

  int get totalCost {
    var cost = 0;
    cartSummary.keys.forEach((productName) {
      cost += ((cartSummary[productName]?.first.price ?? 0) *
          (cartSummary[productName]?.length ?? 0));
    });
    return cost;
  }

  void clearCart() {
    cart = [];
    notifyListeners();
  }
}
