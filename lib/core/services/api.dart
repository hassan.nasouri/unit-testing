import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:unit_testing/core/models/product.dart';
import 'package:unit_testing/core/services/url.dart';

class API {
  static const endpoint = ConstantUrl.productListUrl;

  http.Client client = new http.Client();

  Future<List<Product>> getProducts() async {
    List<Product> products = [];
    final Uri uri = Uri.parse(endpoint + 'products.json');
    final response = await client.get(uri);
    final data = json.decode(response.body) as List<dynamic>;
    for (var product in data) {
      products.add(Product.fromJson(product));
    }
    return products;
  }
}
