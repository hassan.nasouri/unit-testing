import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:unit_testing/core/helpers/dependency_assembly.dart';
import 'package:unit_testing/core/viewmodels/base_model.dart';

class BaseView<T extends BaseModel> extends StatefulWidget {
  final Widget Function(BuildContext context, T value, Widget? child) builder;
  final Function(T)? onModelReady;
  final T? viewModel;

  const BaseView({required this.builder, this.onModelReady, this.viewModel});

  @override
  _BaseViewState<T> createState() => _BaseViewState<T>();
}

class _BaseViewState<T extends BaseModel> extends State<BaseView<T>> {
  T model = dependencyAssembler<T>();

  @override
  void initState() {
    if (widget.onModelReady != null) {
      widget.onModelReady!(model);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.viewModel != null
        ? ChangeNotifierProvider<T>.value(
            value: widget.viewModel!,
            child: widget.builder(context, model, null),
          )
        : ChangeNotifierProvider<T>(
            create: (context) => model,
            child: widget.builder(context, model, null));
  }
}
