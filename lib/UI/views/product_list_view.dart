import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:unit_testing/UI/widgets/cart_count_badge.dart';
import 'package:unit_testing/UI/widgets/product_list.dart';
import 'package:unit_testing/core/constants/strings_constant.dart';
import 'package:unit_testing/core/enums/view_state.dart';
import 'package:unit_testing/core/viewmodels/cart_model.dart';
import 'package:unit_testing/core/viewmodels/product_list_model.dart';
import 'base_view.dart';
import 'cart_view.dart';

class ProductListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseView<CartModel>(
      onModelReady: (cartModel) {
        return cartModel.getCart();
      },
      builder: (context, cartModel, child) {
        return BaseView<ProductListModel>(
          onModelReady: (model) async {
            return await model.getProducts();
          },
          builder: (context, model, child) {
            return Selector<ProductListModel, ViewState>(
                selector: (context, model) => model.state,
                builder: (context, state, _) {
                  return Scaffold(
                    backgroundColor: Colors.grey.shade50,
                    appBar: AppBar(
                      title: Text(ConstantStrings.carListViewTitle),
                      actions: <Widget>[_buildCartButton(context, cartModel)],
                    ),
                    body: model.state == ViewState.Busy
                        ? Center(child: CircularProgressIndicator())
                        : ProductList(model.products!, cartModel),
                  );
                });
          },
        );
      },
    );
  }

  Widget _buildCartButton(BuildContext context, CartModel cartModel) {
    return Stack(
      children: <Widget>[
        IconButton(
            icon: Icon(Icons.shopping_cart),
            splashColor: Colors.blue,
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                    builder: (context) => CartView(model: cartModel)),
              );
            }),
        cartModel.cartSize != 0
            ? CartCountBadge(cartModel.cartSize)
            : Container()
      ],
    );
  }
}
