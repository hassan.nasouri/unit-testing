# unit_testing

It is a simple shopping app with following functionalities. 
- View a list of products from Firebase Realtime Database
- Add product(s) to a shopping cart
- Perform a cart checkout

 `Flutter 2.5.0` `Dart 2.13.4`

## Project Objective

Demonstrating Unit Testing in flutter apps.

you’ll learn how to write unit tests for a flutter app.

## Target Users

Carsome Mobile Team

## Target Platforms

mobile: `android` & `iOS`

## Project Structure

The `Product` class, under **models**, is a simple model object which represents a shopping list product item.

In **services**, you’ll find an `API` class that handles making a network call to fetch a list of products.

Under **viewmodels**, you’ll find three important classes:

A `BaseModelclass` that inherits from `ChangeNotifier`, which uses the `Observer` pattern to notify listeners of any changes in the model. Calling `notifyListeners()` rebuilds the widget tree, allowing the UI to react as the model is updated.

`CartModel` extends and inherits `BaseModel` so it can be easily observed by the UI. You’ll flesh out this class later on.

Finally, `ProductListModel` is a container for a list of products.

In **helpers**, you’ll find important constants in **constants.dart**. You’ll also see **dependency_assembly.dart**, where the dependencies are setup using **GetIt**, a dependency injection package.

The last important piece of code is the `BaseView` class under **ui/views**. `BaseView` is a wrapper widget which contains the `provider` state management logic. It also contains a `model` property that you’ll inject as a dependency.

The base widget is wrapped in a `ChangeNotifierProvider`, which listens to a `ChangeNotifier`, exposes it to its descendants and rebuilds dependencies whenever you call `ChangeNotifier.notifyListeners`.

Finally, switch to **widgets**, where you’ll find a few UI-related widgets that build the **Cart Screen** and the **Product List Screen**. Read through them now to understand how they are implemented.

## What is UnitTesting?

Unit testing is a process where you check quality, performance or reliability by writing extra testing code that ensures your app logic works the way you expect before putting it into widespread use.

Unit testing means you test individual units of code without external dependencies to keep the integrity of each function or method pure. By mocking your data models and classes, you remove dependencies from your tests.

### Mocking

If you made real network calls in your unit tests, your test suite could fail due to an internet problem. To avoid this, you’ll mock API so you can dictate its response.

### A

created a new `MockAPI` class that extends the real **API**.

hardcode two `products`, This makes the function return the hard-coded data instead of downloading live data.

### B

The test suite runs separately from main() in **main.dart**, so it needs to call `setupDependencyAssembler()` to inject dependencies.

### C

Flutter uses the common `group` and `test` paradigm for writing unit tests.

- group function is an event that happened
- test function is the result that you want to ensure
- many test blocks within a group closure

Two of the test cases:

- **Test #1** When the user confirms the purchase, it should show the total cost of all of the items in the cart.

- **Test #2** When the user has finished the purchase, it should clear the cart.

### D

How to run Unit Test?

```
terminal:\> flutter test
```
_It may takes few minutes to complete the task_

expected output: “All tests passed!”

